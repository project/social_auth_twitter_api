<?php

namespace Drupal\social_auth_twitter_api\Controller;

use Drupal\social_auth_decoupled\SocialAuthDecoupledTrait;
use Drupal\social_auth_twitter\Controller\TwitterAuthController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Post login responses for Social Auth Twitter.
 */
class TwitterAuthHttpController extends TwitterAuthController {

  use SocialAuthDecoupledTrait;

  /**
   * Logs in a user.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   A response which contains the ID and CSRF token.
   */
  public function loginByTwitter(Request $request) {
    // @todo Waiting to be done.
    $encoded_response_data = $this->serializer()
      ->encode(['This module hasn\' complete, Please don\'t use it now!'], 'json');
    return new Response($this->serializer()
      ->encode($encoded_response_data, 'json'));
  }

}
